resource "azurerm_virtual_machine_extension" "broker_dcjoin" {
  count                = "${var.broker_instances}"
  name                 = "${local.resource_prefix}-broker-${count.index +1 }-dcjoin"
  location             = "${var.region}"
  depends_on           = ["azurerm_virtual_machine.broker_host"]
  resource_group_name  = "${azurerm_resource_group.resource_group.name}"
  virtual_machine_name = "${element(azurerm_virtual_machine.broker_host.*.name, count.index +1 )}"
  publisher            = "Microsoft.Compute"
  type                 = "JsonADDomainExtension"
  type_handler_version = "1.0"

  settings = <<SETTINGS
  {
    "Name": "${data.terraform_remote_state.domain.domain_name}",
    "User": "${data.terraform_remote_state.domain.domain_name}\\${data.terraform_remote_state.domain.domain_adminuser}",
    "Restart": "true",
    "Options" :  "3"
}
SETTINGS

  protected_settings = <<SETTINGS
{
                "Password":"${data.terraform_remote_state.domain.domain_adminpass}"
                }
SETTINGS
}
