resource "azurerm_virtual_machine_extension" "SQL_Iaas_Extension" {
  name                       = "SqlIaasExtension"
  location                   = "${var.region}"
  depends_on                 = ["azurerm_virtual_machine.sql01_host"]
  resource_group_name        = "${azurerm_resource_group.resource_group.name}"
  virtual_machine_name       = "${local.resource_prefix}-sql-1"
  publisher                  = "Microsoft.SqlServer.Management"
  type                       = "SqlIaaSAgent"
  type_handler_version       = "1.2"
  auto_upgrade_minor_version = true

  settings = <<SETTINGS
    {
    "AutoPatchingSettings": {
      "PatchCategory": "WindowsMandatoryUpdates",
      "Enable": true,
      "DayOfWeek": "Sunday",
      "MaintenanceWindowStartingHour": "0",
      "MaintenanceWindowDuration": "60"
    },
    "AutoBackupSettings": {
         "Enable": true,
      "RetentionPeriod": "30",
      "EnableEncryption": true,
      "BackupSystemDbs" : true,
      "BackupScheduleType" : "automated"
      }
    }
SETTINGS

  protected_settings = <<SETTINGS
{
  "StorageUrl": "${azurerm_storage_account.mssql_storage.primary_blob_endpoint}",
  "StorageAccessKey": "${azurerm_storage_account.mssql_storage.primary_access_key}",
  "Password": "${var.backup_password}"
}
  SETTINGS
}
