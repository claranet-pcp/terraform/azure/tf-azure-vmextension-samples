resource "azurerm_virtual_machine_extension" "jumpbox_ext" {
  name                 = "${var.resource_prefix}-jumpbox-ext"
  location             = "${var.location}"
  depends_on           = ["azurerm_virtual_machine.jumpbox_host"]
  resource_group_name  = "${var.resource_group}"
  virtual_machine_name = "${var.resource_prefix}-jumpbox"
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.7"

  settings = <<SETTINGS
    {
    }
    SETTINGS

  protected_settings = <<SETTINGS
{
    "commandToExecute": "powershell.exe Add-WindowsFeature RSAT-Clustering, NET-Framework-Core, RSAT-SMTP,RSAT-Clustering, RSAT-AD-Tools, RSAT-RDS-Tools, RSAT-DNS-Server, GPMC, Web-Mgmt-Tools, RSAT-Feature-Tools-BitLocker" 
}
  SETTINGS
}
