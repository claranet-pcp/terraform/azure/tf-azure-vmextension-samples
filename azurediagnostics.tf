data "template_file" "xml" {
  template = "${file("windows-config.xml.tpl")}"
}

resource "azurerm_virtual_machine_extension" "test" {
  name                 = "hostname"
  location             = "${azurerm_resource_group.test.location}"
  resource_group_name  = "${azurerm_resource_group.test.name}"
  virtual_machine_name = "${azurerm_virtual_machine.test.name}"
  publisher            = "Microsoft.Azure.Diagnostics"
  type                 = "IaaSDiagnostics"
  type_handler_version = "1.5"

  settings = <<SETTINGS
    {
        "xmlCfg": "${base64encode(data.template_file.xml.rendered)}",
        "storageAccount": "${azurerm_storage_account.test.name}"
    }
SETTINGS

  protected_settings = <<SETTINGS
    {
        "storageAccountName": "${azurerm_storage_account.test.name}",
        "storageAccountKey": "${azurerm_storage_account.test.primary_access_key}"
    }
SETTINGS

  tags {
    environment = "Production"
  }
}
