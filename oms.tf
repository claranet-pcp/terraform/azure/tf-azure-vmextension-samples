resource "azurerm_virtual_machine_extension" "oms_ext_broker" {
  count                      = "${var.broker_instances}"
  name                       = "${local.resource_prefix}-broker-${count.index +1 }-oms-ext"
  location                   = "${var.region}"
  depends_on                 = ["azurerm_virtual_machine.broker_host"]
  resource_group_name        = "${azurerm_resource_group.resource_group.name}"
  virtual_machine_name       = "${element(azurerm_virtual_machine.broker_host.*.name, count.index)}"
  publisher                  = "Microsoft.EnterpriseCloud.Monitoring"
  type                       = "MicrosoftMonitoringAgent"
  type_handler_version       = "1.0"
  auto_upgrade_minor_version = true

  settings = <<SETTINGS
    {
      "workspaceId": "${data.terraform_remote_state.network.oms_workspace_id}"
    }
    SETTINGS

  protected_settings = <<SETTINGS
{
"workspaceKey": "${data.terraform_remote_state.network.oms_workspace_key}"
}
  SETTINGS
}
